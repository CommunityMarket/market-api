/* tslint:disable */
import { Injectable } from '@angular/core';
import { Category } from '../../models/Category';
import { Subcategory } from '../../models/Subcategory';
import { CommunityUser } from '../../models/CommunityUser';
import { Product } from '../../models/Product';
import { Order } from '../../models/Order';
import { Price } from '../../models/Price';
import { Thread } from '../../models/Thread';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    Category: Category,
    Subcategory: Subcategory,
    CommunityUser: CommunityUser,
    Product: Product,
    Order: Order,
    Price: Price,
    Thread: Thread,
    
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
