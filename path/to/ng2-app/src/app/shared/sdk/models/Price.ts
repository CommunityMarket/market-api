/* tslint:disable */

declare var Object: any;
export interface PriceInterface {
  "currency": string;
  "amount": number;
  "id"?: number;
}

export class Price implements PriceInterface {
  "currency": string;
  "amount": number;
  "id": number;
  constructor(data?: PriceInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Price`.
   */
  public static getModelName() {
    return "Price";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Price for dynamic purposes.
  **/
  public static factory(data: PriceInterface): Price{
    return new Price(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Price',
      plural: 'prices',
      path: 'prices',
      properties: {
        "currency": {
          name: 'currency',
          type: 'string'
        },
        "amount": {
          name: 'amount',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
