/* tslint:disable */

declare var Object: any;
export interface ProductInterface {
  "id"?: string;
  "name": string;
  "image": string;
  "description": string;
  "deliveryDays": Array<any>;
  "prices": Array<any>;
  "ingredients": Array<any>;
  "location": string;
}

export class Product implements ProductInterface {
  "id": string;
  "name": string;
  "image": string;
  "description": string;
  "deliveryDays": Array<any>;
  "prices": Array<any>;
  "ingredients": Array<any>;
  "location": string;
  constructor(data?: ProductInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Product`.
   */
  public static getModelName() {
    return "Product";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Product for dynamic purposes.
  **/
  public static factory(data: ProductInterface): Product{
    return new Product(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Product',
      plural: 'products',
      path: 'products',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "name": {
          name: 'name',
          type: 'string'
        },
        "image": {
          name: 'image',
          type: 'string'
        },
        "description": {
          name: 'description',
          type: 'string'
        },
        "deliveryDays": {
          name: 'deliveryDays',
          type: 'Array&lt;any&gt;'
        },
        "prices": {
          name: 'prices',
          type: 'Array&lt;any&gt;'
        },
        "ingredients": {
          name: 'ingredients',
          type: 'Array&lt;any&gt;'
        },
        "location": {
          name: 'location',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
