/* tslint:disable */

declare var Object: any;
export interface ThreadInterface {
  "id"?: string;
  "subject": string;
  "started": Date;
}

export class Thread implements ThreadInterface {
  "id": string;
  "subject": string;
  "started": Date;
  constructor(data?: ThreadInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Thread`.
   */
  public static getModelName() {
    return "Thread";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Thread for dynamic purposes.
  **/
  public static factory(data: ThreadInterface): Thread{
    return new Thread(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Thread',
      plural: 'threads',
      path: 'threads',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "subject": {
          name: 'subject',
          type: 'string'
        },
        "started": {
          name: 'started',
          type: 'Date'
        },
      },
      relations: {
      }
    }
  }
}
