/* tslint:disable */
import {
  Subcategory
} from '../index';

declare var Object: any;
export interface CategoryInterface {
  "id"?: string;
  "description": string;
  "_subcategories"?: Array<any>;
  subcategories?: Subcategory[];
}

export class Category implements CategoryInterface {
  "id": string;
  "description": string;
  "_subcategories": Array<any>;
  subcategories: Subcategory[];
  constructor(data?: CategoryInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Category`.
   */
  public static getModelName() {
    return "Category";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Category for dynamic purposes.
  **/
  public static factory(data: CategoryInterface): Category{
    return new Category(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Category',
      plural: 'categories',
      path: 'categories',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "description": {
          name: 'description',
          type: 'string'
        },
        "_subcategories": {
          name: '_subcategories',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
      },
      relations: {
        subcategories: {
          name: 'subcategories',
          type: 'Subcategory[]',
          model: 'Subcategory'
        },
      }
    }
  }
}
