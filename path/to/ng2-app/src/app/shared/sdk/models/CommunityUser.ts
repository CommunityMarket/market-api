/* tslint:disable */

declare var Object: any;
export interface CommunityUserInterface {
  "id"?: string;
  "firstName": string;
  "lastName": string;
  "dayOfBirth"?: Date;
  "email": string;
  "role": string;
  "cellphone": string;
  "gender"?: string;
  "image"?: string;
  "location": string;
  "avgRating": number;
  "realm"?: string;
  "username"?: string;
  "emailVerified"?: boolean;
  "password"?: string;
  accessTokens?: any[];
}

export class CommunityUser implements CommunityUserInterface {
  "id": string;
  "firstName": string;
  "lastName": string;
  "dayOfBirth": Date;
  "email": string;
  "role": string;
  "cellphone": string;
  "gender": string;
  "image": string;
  "location": string;
  "avgRating": number;
  "realm": string;
  "username": string;
  "emailVerified": boolean;
  "password": string;
  accessTokens: any[];
  constructor(data?: CommunityUserInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `CommunityUser`.
   */
  public static getModelName() {
    return "CommunityUser";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of CommunityUser for dynamic purposes.
  **/
  public static factory(data: CommunityUserInterface): CommunityUser{
    return new CommunityUser(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'CommunityUser',
      plural: 'CommunityUsers',
      path: 'CommunityUsers',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "firstName": {
          name: 'firstName',
          type: 'string'
        },
        "lastName": {
          name: 'lastName',
          type: 'string'
        },
        "dayOfBirth": {
          name: 'dayOfBirth',
          type: 'Date'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "role": {
          name: 'role',
          type: 'string'
        },
        "cellphone": {
          name: 'cellphone',
          type: 'string'
        },
        "gender": {
          name: 'gender',
          type: 'string'
        },
        "image": {
          name: 'image',
          type: 'string'
        },
        "location": {
          name: 'location',
          type: 'string'
        },
        "avgRating": {
          name: 'avgRating',
          type: 'number',
          default: 5
        },
        "realm": {
          name: 'realm',
          type: 'string'
        },
        "username": {
          name: 'username',
          type: 'string'
        },
        "emailVerified": {
          name: 'emailVerified',
          type: 'boolean'
        },
        "password": {
          name: 'password',
          type: 'string'
        },
      },
      relations: {
        accessTokens: {
          name: 'accessTokens',
          type: 'any[]',
          model: ''
        },
      }
    }
  }
}
