/* tslint:disable */

declare var Object: any;
export interface SubcategoryInterface {
  "id"?: string;
  "description": string;
}

export class Subcategory implements SubcategoryInterface {
  "id": string;
  "description": string;
  constructor(data?: SubcategoryInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Subcategory`.
   */
  public static getModelName() {
    return "Subcategory";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Subcategory for dynamic purposes.
  **/
  public static factory(data: SubcategoryInterface): Subcategory{
    return new Subcategory(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Subcategory',
      plural: 'subcategories',
      path: 'subcategories',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "description": {
          name: 'description',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
