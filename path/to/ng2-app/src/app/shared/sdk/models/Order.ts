/* tslint:disable */

declare var Object: any;
export interface OrderInterface {
  "id"?: string;
  "buyer": string;
  "seller": string;
  "products": Array<any>;
  "notes": string;
  "status": string;
  "location": string;
  "deliveryMethod": string;
}

export class Order implements OrderInterface {
  "id": string;
  "buyer": string;
  "seller": string;
  "products": Array<any>;
  "notes": string;
  "status": string;
  "location": string;
  "deliveryMethod": string;
  constructor(data?: OrderInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Order`.
   */
  public static getModelName() {
    return "Order";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Order for dynamic purposes.
  **/
  public static factory(data: OrderInterface): Order{
    return new Order(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Order',
      plural: 'orders',
      path: 'orders',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "buyer": {
          name: 'buyer',
          type: 'string'
        },
        "seller": {
          name: 'seller',
          type: 'string'
        },
        "products": {
          name: 'products',
          type: 'Array&lt;any&gt;'
        },
        "notes": {
          name: 'notes',
          type: 'string'
        },
        "status": {
          name: 'status',
          type: 'string'
        },
        "location": {
          name: 'location',
          type: 'string'
        },
        "deliveryMethod": {
          name: 'deliveryMethod',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
